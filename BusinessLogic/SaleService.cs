﻿using System;
using System.Linq;

namespace SugarSale.BusinessLogic
{
    public class SaleService : ISaleService, IService
    {
        public float[] Sale(float summaryWeight, float[] packagesWeight)
        {
            float temp = 0;

            if (packagesWeight == null)
            {
                throw new ArgumentNullException(nameof(packagesWeight));
            }

            if (summaryWeight <= 0 && packagesWeight.Length == 0 && packagesWeight.Length == 1)
            {
                throw new ArgumentException("One of the both argument is not valid");
            }

            if (packagesWeight.Any(p => p < 0))
            {
                throw new ArgumentException("Contains no valid value", nameof(packagesWeight));
            }

            for (int i = 0; i < packagesWeight.Length; i++)
            {
                var j = i;
                temp = packagesWeight[i] + packagesWeight[i + 1];
                if (temp != summaryWeight)
                {
                    for (int k = i+1; k < packagesWeight.Length;)
                    {
                        temp = packagesWeight[i] + packagesWeight[k + 1];

                    }
                }
            }

            return null;
        }

        public void Buy(int id)
        {

        }


    }
}